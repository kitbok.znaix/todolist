let id = "no";

selectData(); // call to display data

// When ENTER key is press it will trigger Submit button
document.getElementById("name").addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault(); // Prevent the default form submission
    manageData(); // Call your submit function
  }
});

// Insert & Update Data (SUBMIT BUTTON)
function manageData() {
  document.getElementById("msg").innerHTML = "";
  let sname = document.getElementById("name").value;
  const name = sname.replace(/[^a-zA-Z0-9]/g, ""); // Sanitize
  if (name == "") {
    document.getElementById("msg").innerHTML = "Please enter Task";
  } else {
    console.log(id);
    if (id == "no") {
      let arr = getCrudData();
      if (arr == null) {
        let data = [name];
        setCrudData(data);
      } else {
        arr.push(name);
        setCrudData(arr);
      }
      document.getElementById("msg").innerHTML = "Data added";
    } else {
      let arr = getCrudData();
      arr[id] = name;
      setCrudData(arr);
      document.getElementById("msg").innerHTML = "Data updated";

      // Change Class and value of id=Submit
      let submitBtn = document.getElementById("submit");
      submitBtn.value = "Submit"; // change Update -> Submit
      submitBtn.className =
        "bg-green-500 hover:bg-green-700 	text-white font-bold py-2 px-4 rounded-full";
      // change input border color id=name
      let inputBtn = document.getElementById("name");
      inputBtn.className = "p-2 mr-6 my-10 border border-green-500";
    }
    document.getElementById("name").value = "";
    selectData();
  }
}

// display Data
function selectData() {
  let arr = getCrudData();
  if (arr != null) {
    let html = "";
    let sno = 1;
    for (let k in arr) {
      html =
        html +
        `<tr class="bg-red-50 text-center text-xl"><td>${sno}</td><td>${arr[k]}</td><td><a href="javascript:void(0)" onclick="editData(${k})" class="font-bold text-blue-500 mr-2 text-2xl hover:underline"><i class="fa fa-edit"></i></a>&nbsp;<a href="javascript:void(0)" onclick="deleteData(${k})" class="font-bold text-red-500 text-2xl hover:underline"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>`;
      sno++;
    }
    document.getElementById("root").innerHTML = html;
  }
  // Total number Task
  document.getElementById("total").innerHTML = arr.length;
}

// Update button
function editData(rid) {
  id = rid;
  let arr = getCrudData();
  document.getElementById("name").value = arr[rid];
  // change Submit -> Update
  let submitBtn = document.getElementById("submit");
  submitBtn.value = "Update";
  submitBtn.className =
    "bg-blue-500 hover:bg-blue-700 	text-white font-bold py-2 px-4 rounded-full";
  // change input border color id=name
  let inputBtn = document.getElementById("name");
  inputBtn.className = "p-2 mr-6 my-10 border border-blue-500";
}

// delete Button
function deleteData(rid) {
  let arr = getCrudData();
  arr.splice(rid, 1);
  setCrudData(arr);
  selectData();
  document.getElementById("msg").innerHTML = "Data deleted";
}

// retrive the value
function getCrudData() {
  let arr = JSON.parse(localStorage.getItem("crud"));
  return arr;
}

// save data
function setCrudData(arr) {
  localStorage.setItem("crud", JSON.stringify(arr));
}
